## Conceptos básicos:
- Repositorio: es como una carpeta en la que se encuentran
archivados los ficheros de tu proyecto: código, documentación
o ejemplos.
Tipos de repositorios: Público (visible para todos) o privado
(acceso restringido para usuarios específicos)
- Archivo README: Contiene instrucciones básicas para
moverse en tu repositorio.
Nombre del proyecto, Descripción/Créditos, Índice de
contenidos, Uso del proyecto, Licencia, etc.
- RAMA: Realmente son copias del contenido de un proyecto.
- Rama master: contiene el proyecto original.
- Rama remota: Copia del proyecto original en la que
trabajamos paralelamente.
Es una copia en local, offline, que utilizo para ir avanzado en
mi proyecto. Un error en esta rama no afectaría al proyecto
original.
- Clone/fork: son instantáneas que hacemos del repositorio.
- Clone: copia del proyecto original para trabajar local (offline).
- Fork: copia del proyecto original creada en tu perfil de GitHub
para trabajar online.
- COMMIT:
A medida que vamos haciendo cambios, modificando ficheros,
añadiendo nuevas líneas, borrando existentes, etc, vamos
guardando los cambios en forma de “commits” (registro o
foto)
Se guarda en la rama local.