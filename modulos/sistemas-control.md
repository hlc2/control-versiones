## ¿Qué es el control de versiones?

El control de versiones es un sistema que registra los cambios realizados
sobre un archivo o conjunto de archivos a lo largo del tiempo, de modo que
puedas recuperar versiones específicas más adelante.
Te permite revertir archivos a un estado anterior, revertir el proyecto entero a
un estado anterior, comparar cambios a lo largo del tiempo, ver quién modificó
por última vez algo que puede estar causando un problema, quién introdujo un
error y cuándo, y mucho más.
Usar un VCS también significa generalmente que si fastidias o pierdes
archivos, puedes recuperarlos fácilmente.

- VCS locales:
Contenían una simple base de datos
en la que se llevaba registro de
todos los cambios realizados sobre
los archivos.


- VCS Centralizado:
Tienen un único servidor que
contiene todos los archivos
versionados, y los colaboradores se
descargan los archivos de ese lugar
central.


- VCS distribuido:
Los colaboradores no sólo
descargan la última instantánea de
los archivos, replican
completamente el repositorio.
Así, si un servidor muere, y estos
sistemas estaban colaborando a
través de él, cualquiera de los
repositorios de los colaboradores
puede copiarse en el servidor para
restaurarlo.

